# README #

Bienvenue sur notre Repository Bitbucket, pour notre projet pour le cours d'Interaction Homme Machine.
Nous y développons une application mobile, intitulée **Fai'TaFête**, qui sera implémentée sur le framework **Quasar**

### Membres du groupe ###
* CUSIN Suzy
* RAZANAMARIMANDIMBY Maminiaina

### Les rendus du projet ###
(Liens de téléchargement)

* [Le cahier des charges](https://bitbucket.org/MaminiainaRAZA/faitafete/downloads/Cahier_des_charges.pdf)
* [Les scénarii](https://bitbucket.org/MaminiainaRAZA/faitafete/downloads/Les_sc%C3%A9narii.pdf)
* [Le modèle utilisateur](https://bitbucket.org/MaminiainaRAZA/faitafete/downloads/Mod%C3%A8le_Utilisateur.pdf)